const basedate = new Date("05-06-2022");
const today = new Date()
var timediff = today.getTime() - basedate.getTime();
var daydiff = timediff / (1000 * 3600 * 24);
var floored = Math.floor(daydiff);
var todaychal = solutions[floored];
var canstillplay = true;

let i = 0;
document.querySelectorAll(".key.num").forEach((e) => {
    e.innerHTML = todaychal["but"][i];
    e.setAttribute("num-value", todaychal["but"][i])
    i++;
})

/* 
document.querySelector(`.giveawayrow > input:nth-child(${todaychal["giveaway"][0]})`).value = todaychal["giveaway"][1];
document.querySelector(`.giveawayrow > input:nth-child(${todaychal["giveaway"][0]})`).classList.remove("nodisplay")
*/

var currentrow = 2;
var currentindex = 1;
var type = "num";

document.querySelectorAll(".key").forEach((e) => {
    console.log(e)
    e.addEventListener("click", function(e2) {
        if (canstillplay && currentrow <= 7) {
            if (e2.target.innerHTML == "⌫") {
                if (currentindex > 1) {
                    currentindex--;
                    document.querySelector(`.row:nth-child(${currentrow}) > input:nth-child(${currentindex})`).value = "";
                    console.log(currentindex)
                    if (currentindex <= 0) {
                        currentindex = 1;
                    }
                    console.log(currentindex)
                    swap();
                }
            } else {
                if (currentindex <= 7) {
                    document.querySelector(`.row:nth-child(${currentrow}) > input:nth-child(${currentindex})`).value = e2.target.innerHTML;
                    if (type == "num") {
                        type = "equ";
                    } else {
                        type = "num";
                    }
                    swap();
                    currentindex++;
                }
            }
        }
    })
})

function swap() {
    document.querySelectorAll(".active, .inactive").forEach((e3) => {
        if (e3.classList.contains("active")) {
            e3.classList.replace("active", "inactive");
        } else {
            e3.classList.replace("inactive", "active");
        }
    })
}

function enter() {
    var stringedequ = document.querySelector(`.row:nth-child(${currentrow}) > input:nth-child(1)`).value + document.querySelector(`.row:nth-child(${currentrow}) > input:nth-child(2)`).value + document.querySelector(`.row:nth-child(${currentrow}) > input:nth-child(3)`).value + document.querySelector(`.row:nth-child(${currentrow}) > input:nth-child(4)`).value + document.querySelector(`.row:nth-child(${currentrow}) > input:nth-child(5)`).value + document.querySelector(`.row:nth-child(${currentrow}) > input:nth-child(6)`).value + document.querySelector(`.row:nth-child(${currentrow}) > input:nth-child(7)`).value;
    if (stringedequ.includes("=") && rowCheck()) {
        stringedequ = stringedequ.replace(/=/g, "==").replace(/×/g, "*").replace(/÷/g, "/");
        console.log(stringedequ);
        if (eval(stringedequ)) {
            reveal()
        } else {
            showNotification("invalid equation!")
        }
    } else {
        showNotification("invalid equation!")
    }
    var correct = 0;
    document.querySelectorAll(`.row:nth-child(${currentrow - 1}) > input`).forEach((e) => {
        if (e.classList.contains("correct")) {
            correct++
        }
    })
    if (correct == 7) {
        let colors = [
            "#FF6F6F",
            "#FFB06F",
            "#FFF26F",
            "#6FFF85",
            "#6FEDFF",
            "#6F8CFF",
            "#9E6FFF"
        ]
        let color = 0;
        document.querySelectorAll(`.row:nth-child(${currentrow - 1}) > input`).forEach((e) => {
            e.classList.remove("close");
            e.classList.remove("correct");
            e.classList.remove("incorrect");
            e.style.background = colors[color]
            color++;
        })
        canstillplay = false;
        uwuMckennaSucks();
        if (cookies() !== "undefined") {
            if (cookies()["lastplayed"] !== floored) {
                setStats(true, currentrow - 2);
            }
        } else {
            setStats(true, currentrow - 2);
        }
        showStats();
        finalScreen("YOU WON!");
    } else if (currentrow == 8) {
        canstillplay = false;
        uwuMckennaSucks();
        if (cookies() !== "undefined") {
            if (cookies()["lastplayed"] !== floored) {
                setStats(false, 6);
            }
        } else {
            setStats(false, 6);
        }
        showStats();
        finalScreen("you lose :(");
    }
}

function reveal() {
    let i = 0;
    let yellows = [];
    document.querySelectorAll(`.row:nth-child(${currentrow}) > input`).forEach((e) => {
        console.log(todaychal["solapart"][i])
        console.log(e.value)
        if (todaychal["solapart"][i] == e.value) {
            e.classList.add("correct");
            document.querySelector(`.key[num-value="${e.value}"]`).classList.remove("wrong");
            document.querySelector(`.key[num-value="${e.value}"]`).classList.remove("close");
            document.querySelector(`.key[num-value="${e.value}"]`).classList.add("correct");
            yellows.push(e.value);
        }
        i++;
    })
    i = 0
    document.querySelectorAll(`.row:nth-child(${currentrow}) > input`).forEach((e) => {
        console.log(todaychal["solapart"][i])
        console.log(e.value)
        if (todaychal["solapart"].includes(e.value)) {
            if (!yellows.includes(e.value)) {
                e.classList.add("close");
                document.querySelector(`.key[num-value="${e.value}"]`).classList.remove("wrong");
                document.querySelector(`.key[num-value="${e.value}"]`).classList.remove("correct");
                document.querySelector(`.key[num-value="${e.value}"]`).classList.add("close");
                yellows.push(e.value)
            } else {
                if (!e.classList.contains("correct")) {
                    e.classList.add("wrong")
                }
            }
        } else {
            e.classList.add("wrong");
            document.querySelector(`.key[num-value="${e.value}"]`).classList.remove("correct");
            document.querySelector(`.key[num-value="${e.value}"]`).classList.remove("close");
            document.querySelector(`.key[num-value="${e.value}"]`).classList.add("wrong");
        }
        i++;
    })
    currentrow++;
    currentindex = 1;
    swap();
}

function showNotification(text) {
    const element = document.createElement("div");
    // const element2 = document.createElement("h2");
    const etext = document.createTextNode(text);
    element.classList.add("notification");
    element.classList.add("centeritems");
    element.appendChild(etext);
    // element.appendChild(element2)
    document.querySelector("#notificationbox").appendChild(element);
}

function rowCheck() {
    var full = 0;
    document.querySelectorAll(`.row:nth-child(${currentrow}) > input`).forEach((e) => {
        if (!e.value == "") {
            full++;
        }
    })
    if (full == 7) {
        return true;
    } else {
        return false;
    }
}

function finalScreen(text) {
    document.querySelector("#finallightbox").classList.add("showing");
    document.querySelector("#finallightbox h1").innerHTML = text;
    
}

function closelightbox() {
    document.querySelector("#finallightbox").classList.remove("showing");
}

document.getElementById("closelightbox").addEventListener("click", (e) => {
    closelightbox()
});

function uwuMckennaSucks() {
    document.querySelectorAll(".key, .enter").forEach((e) => {
        e.classList.remove("active");
        e.classList.add("inactive");
    })
}

function cookies() {
    try {
        return JSON.parse(document.cookie);
    } catch {
        return "undefined";
    }
}

function setStats(correct, tries) {
    let cookiesJSON;
    console.log()
    if (cookies() == "undefined") {
        cookiesJSON = {};
    } else {
        cookiesJSON = cookies();
    }
    console.log(cookiesJSON)
    cookiesJSON["lastplayed"] = floored;
    if (typeof cookiesJSON["tries"] == "undefined") {
        cookiesJSON["tries"] = [];
    }
    if (typeof cookiesJSON["correct"] == "undefined") {
        cookiesJSON["correct"] = 0;
        cookiesJSON["incorrect"] = 0;
        cookiesJSON["timesplayed"] = 0;
    }
    if (correct) {
        cookiesJSON["correct"]++;
        cookiesJSON["tries"].push(tries);
    } else {
        cookiesJSON["incorrect"]++;
    }
    cookiesJSON["timesplayed"]++;
    document.cookie = JSON.stringify(cookiesJSON);
    /* let cr = 1;
    let list = [[], [], [], [], [], []]
    document.querySelectorAll(`.row:nth-child(${cr})`).forEach((e) => {
        document.querySelectorAll(`.row:nth-child(${cr}) > input`).forEach((e2) => {
            list[cr - 1].push(e2.class)
        })
    }) */
}

function showStats() {
    let stats = cookies();
    if (stats == "undefined") {
        document.querySelector("#stats").style.display = "none";
        document.querySelector("#finallightbox h1").innerHTML = "you have to play to get stats!";
    }
    document.querySelector("#stats h3[eq-type='timesplayed']").innerHTML = stats["timesplayed"];
    document.querySelector("#stats h3[eq-type='timescorrect']").innerHTML = stats["correct"];
    document.querySelector("#stats h3[eq-type='timesincorrect']").innerHTML = stats["incorrect"];
    document.querySelector("#stats h3[eq-type='correctimeter']").innerHTML = String(Math.round(stats["correct"] / stats["timesplayed"] * 100)) + "%";
}

/* $(window).load(function() {
  $("body").removeClass("preload");
}); */

window.onload = function() {
    document.body.classList.remove("preload")
};